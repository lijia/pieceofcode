#include <stdio.h>
#include <uv.h>

int64_t counter = 0;

void wait_for_a_while(uv_idle_t* handle, int status) {
    counter++;

    if (counter >= 1000000)
    {
        uv_idle_stop(handle);
	printf("Exiting...\n");
    }
}

int main() {
    uv_idle_t idler;
    printf("%lu\n", sizeof(uv_idle_t));

    uv_idle_init(uv_default_loop(), &idler);
    uv_idle_start(&idler, wait_for_a_while);

    uv_run(uv_default_loop(), UV_RUN_DEFAULT);

    return 0;
}
