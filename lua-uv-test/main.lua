local ffi = require("ffi")

ffi.cdef[[

typedef struct {char buff[88];} uv_idle_t;
typedef struct {} uv_loop_t;

uv_loop_t* uv_default_loop(void);

typedef void (*uv_idle_cb)(uv_idle_t* handle, int status);

int uv_idle_init(uv_loop_t*, uv_idle_t* idle);

int uv_idle_start(uv_idle_t* idle, uv_idle_cb cb);

int uv_idle_stop(uv_idle_t* idle);

int uv_run(uv_loop_t*, int mode);
]]
local uv = ffi.load("uv")
local idler = ffi.new("uv_idle_t[1]");

uv.uv_idle_init(uv.uv_default_loop(), idler);

    local counter = 0
uv.uv_idle_start(idler,
function(handle, status)
	counter = counter + 1
	if (counter > 1000000) then
	    uv.uv_idle_stop(handle)
	    print("Exiting... ")
	end
end
)
uv.uv_run(uv.uv_default_loop(), 0)
