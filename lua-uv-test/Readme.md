

a different result with
[LuaJIT之callback大坑绕路记](http://www.cppblog.com/tdzl2003/archive/2013/02/24/198045.html)

1. checkout libuv code, assume in `../../libuv`, build it as a shared library
2. link result libuv.so to current dir `link ../../libuv/out/Release/lib.target/libuv.so libuv.so.0.11`
3. use scons build idle example from [idle-basic/main.c](http://nikhilm.github.io/uvbook/basics.html#idling)
4. run `time main` and `time luajit main.lua` 

in my computer

$time ./main

Exiting...
./main  0.07s user 0.09s system 97% cpu 0.160 total


$time luajit main.lua

Exiting... 
luajit ./main.lua  0.23s user 0.08s system 99% cpu 0.310 total


then, lua callback is 3X slower than C, but only 0.14s in 1000000 counts
